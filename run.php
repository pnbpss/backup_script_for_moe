<?php
require_once('config.php');
require_once('_backup.php');
require_once('_ftp.php');

#perform backup database from database server.
$backup = new \App\backup\Backup(); #initialize backup instance.
$backup->perform_backup(); #perform backup, this process may take a few minutes up to size of database.
$backup->manage_file(); #perform delete old backup file of each database, if new backup file of each database have been created.
$backed_up_files = $backup->get_backedup_files(); #get file list,array format, for ftp purpose.

#perform send file to secondary backup server
$ftp = new \App\Backupfile\Ftp(_FTP_SERVER_,_FTP_USER_,_FTP_PASSWORD_); #initialize ftp instance for transfer files
$ftp->chdir('/upload/link'); #change remote ftp directory
$ftp->send_multifile($backed_up_files); #send multiple file 
$ftp->disconnect(); #disconnect ftp server