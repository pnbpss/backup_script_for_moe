<?php
namespace App\backup;
require_once('config.php');
class backup {
    private $files = [];
    public function __construct(){

        if (!file_exists(DB_FILES_PATH)) {
            mkdir(DB_FILES_PATH, 0777, true);
        }

        $dbs_name_str = "'".implode("','",_DB_NAMES_)."'"; # "'','','','',''";

        $sql = "
        /*
        Script written by Panu Boonpromsook. 20210203
        purpose of script is backing up databases to disk
        */
        use master;

        declare @script_name varchar(max), @datetimebak varchar(50),@fileNameAndPath varchar(max),@databaseName varchar(max), @backupName varchar(max), @filePath varchar(max);
        set @script_name = '.by_bps_script';
        set @filePath = '".DB_FILES_PATH."'; 

        declare dbname_cursor cursor for SELECT [name]  FROM sys.databases where [name] in (".$dbs_name_str.");

        open dbname_cursor 
        fetch next from dbname_cursor into @databaseName 

        while @@FETCH_STATUS = 0
        begin
            set @datetimebak = convert(varchar(max),getdate(),112)+replace(convert(varchar(max),getdate(),108),':','');
            set @fileNameAndPath = @filePath+@datetimebak+'_'+@databaseName+ @script_name+'."._BAK_EXT."';
            set @backupName = @databaseName+'-Full Database Backup ("._MARK_ONE_.") '+@datetimebak;
            BACKUP DATABASE @databaseName TO  
            DISK = @fileNameAndPath
            WITH NOFORMAT,
            NOINIT,
            NAME = @backupName,
            SKIP, NOREWIND, NOUNLOAD,  STATS = 5
            fetch next from dbname_cursor into @databaseName 
        end
        GO
        CLOSE dbname_cursor;  
        DEALLOCATE dbname_cursor; 
        GO
        ";

        file_put_contents(SQL_FILE_NAME, $sql);

        $this->path = DB_FILES_PATH;
    }

    public function perform_backup()
    {
        exec('sqlcmd -S '._BACKUP_SERVER_.' -U '._BACKUP_USER_.' -P '._BACKUP_PASSWORD_.' -i '._BACKUP_SCRIPT_FILENAME_.'');
    }

    private function extract_dbfiles(array $all_files){
        $files = array();

        foreach($all_files as $focus_file){	
            $tmp_f = explode(".",$focus_file);
            if(sizeof($tmp_f)<=1){
                continue;
            }
            if($tmp_f[sizeof($tmp_f)-2].$tmp_f[sizeof($tmp_f)-1] ==='by_bps_scriptbak'){
                $files[] = [
                    'name'=>$focus_file,
                    'date_created'=>strtotime(explode("_", $focus_file)[0]),
                    'dbname'=> str_replace('.'._MARK_ONE_.'.'._BAK_EXT,'',substr($focus_file,15,5000)),
                ];
            }
        }
        return $files;
    }

    private function delete_old_files($files){
        foreach($files as $file)
        {
            
            $fileTime = $file['date_created'];
            $currentTime = time();
            $minutesAgo = ceil(($currentTime - $fileTime) / 60); #divide by seconds in a minute;
            $daysAgo = ceil(($currentTime - $fileTime) / MINUTES_IN_A_DAY);
            echo $this->path."".$file['name']." was last created at " . date ("Y/m/d H:i:s", $fileTime);

            echo ', '.$minutesAgo.' minutes ago, or ('.$daysAgo.' day(s) ago.)';

            if($minutesAgo > MINUTES_DIFF_TO_DELETE){
                $path_and_file = $this->path.'\\'.$file['name'];
                echo ' must be deleted, ';
                if(file_exists($path_and_file)){
                    unlink($path_and_file);
                    echo ' DELETED';
                }
            }
            
            echo PHP_EOL;
        }
    }

    public function manage_file()
    {        
        
        $all_files = scandir($this->path); #SCANDIR_SORT_DESCENDING

        $this->files = $this->extract_dbfiles($all_files);

        echo 'File left = '.sizeof($this->files).', must left = '.MUST_LEFT_FILE.PHP_EOL;

        if(	MUST_LEFT_FILE>(sizeof($this->files)) ) {
            exit('File left less than '.MUST_LEFT_FILE.", operation stopped."); #nothing to do, check why backing up dbs not run.
        }

        $this->delete_old_files($this->files);
    }
    public function get_backedup_files(){
        return $this->files;
    }
}