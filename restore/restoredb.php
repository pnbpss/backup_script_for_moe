<?php
#restore database;

require_once('config.php');

$sql_template = "

USE master;
GO
ALTER DATABASE [BACKUP_"._DBNAME_MARK_."] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
GO

RESTORE DATABASE [BACKUP_"._DBNAME_MARK_."] 
FROM  DISK = N'".DB_FILES_PATH._DBNAME_MARK_.".bak' 
WITH  FILE = 1,  NOUNLOAD,  REPLACE,  STATS = 10
GO

ALTER DATABASE [BACKUP_"._DBNAME_MARK_."] SET MULTI_USER;
GO

";

foreach(_DB_NAMES_ as $dbname){
	$sql = str_replace(_DBNAME_MARK_,$dbname, $sql_template);
	
	$script_name = 'scripts\\'.$dbname.'_'.SQL_FILE_NAME;
	
	file_put_contents($script_name, $sql);
	echo PHP_EOL.'Restoring '.$dbname;
	$command = 'sqlcmd -S '._BACKUP_SERVER_.' -U '._BACKUP_USER_.' -P '._BACKUP_PASSWORD_.' -i '.$script_name.'';
	exec($command);
	//echo $command.PHP_EOL;	
	echo PHP_EOL.'Completed restoring '.$dbname;
}
