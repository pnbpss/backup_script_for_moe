<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


/**
 * ชื่อไฟล์ที่จะเก็บสคริปท์สำรอง
 */
define('SQL_FILE_NAME', 'restore.script.sql');

/**
 * path ที่จะเก็บไฟล์สำรอง
 */
define('DB_FILES_PATH',__DIR__.'\\dbfiles\\');

define('_DBNAME_MARK_','###D.B.N.A.M.E###'); 
/**
 * ชื่อ database ที่จะให้ทำการสำรอง
 */
define('_DB_NAMES_', [
	'Car01',
	'Car04',
	'DataDemo',
	'distribution',
	'eOffice',
	'Leave',
	'LMS',
	'meeting',
	'ReportServer$SQLSR08R2_SALARY',
	'Salary',
	'SALARY25-02-2010SQL',
	'SalaryEDU',
]);


/**
 * database configurartion
 */
define('_BACKUP_SERVER_','mssql_server');
define('_BACKUP_USER_','username');
define('_BACKUP_PASSWORD_','password');
