<?php

/**
 * จำนวนนาทีที่ต่างระหว่าง เวลาปัจจุบัน กับเวลาที่ ไฟล์สำรองถูกสร้าง หากไฟล์สำรองนั้นถูกสร้างนานแล้วเกินกว่า MINUTES_DIFF_TO_DELETE ไฟล์สำรองนั้นจะถูกลบ
 * แตการจะลบไฟล์นั้นหรือไม่ขึ้นอยู่กับจำนวนไฟล์คงเหลือด้วย (ดูเพิ่มเติมที่ค่าตัวคงที่ MUST_LEFT_FILE)
 */
define('MINUTES_DIFF_TO_DELETE',3); #in minute; 129600min=90days,86400min=60days,43200min=30days,1440min=1day

/** 
 * จำนวนนาทีใน 1 วัน
 */ 
define('MINUTES_IN_A_DAY', 1440*60); 

/**
 * ชื่อไฟล์ที่จะเก็บสคริปท์สำรอง
 */
define('SQL_FILE_NAME', 'backup.script.sql');

/**
 * path ที่จะเก็บไฟล์สำรอง
 */
define('DB_FILES_PATH',__DIR__.'\\dbfiles\\');

/**
 * ชื่อ database ที่จะให้ทำการสำรอง
 */
define('_DB_NAMES_', [
	'eec_jigsawoffice',
	'tpqi_jigsawoffice',
	'trd_jigsawoffice',
	'trd_web',
	'trd_mini',
	'anamai_web',
	'synerry_complaint',
	'synerry_onesqa_old',
	'synerry_onesqa_new',
	'anamai.covid19',
	'synerry_onesqa_converted',
	'anamai_minisite',
	'ieat_web',
	'industry.law',
	'migrate',
	'industry_web',
	'industry_minisite',
	'industry_ops',
	'distribution',
	'TutorialDB',
]);

/**
 * จำนวนไฟล์ที่จะคงเหลือไว้
 * การสำรองในแต่ละครั้งจะมีไฟล์ใหม่เกิดขึ้น ดังนั้นจำเป็นต้องลบไฟล์เก่าออกด้วย 
 * เงื่อนไขที่จะลบไฟล์เก่าออก คิดจาก ค่าคงที่ MINUTES_DIFF_TO_DELETE ว่าเคยสร้างเมื่อกี่นาทีที่แล้ว  
 * เช่น ค่า MINUTES_DIFF_TO_DELETE กำหนดเป็น 86400นาที(60วัน) นั่นคือไฟล์ที่ถูกสร้างมาก่อน 60 วันจะถูกลง
 * แต่อย่างไรก็ตาม หากไฟล์เหลือน้อยกว่า MUST_LEFT_FILE ไฟล์จะไมถูกลบ ถึงแม้จะสร้างมานานเกิน 60 วัน
 * ทั้งนี้เพื่อป้องกันไฟล์สำรองถูกลบจนเกลี้ยง ในกรณีที่การสำรองไม่ได้เกิดขึ้นมาเป็นเวลาเกิน 60 วัน
 */
define('MUST_LEFT_FILE',sizeof(_DB_NAMES_)*2);

/**
 * คีย์เวิร์ดที่จะเอาไว้ต่อท้ายชื่อไฟล์ จุดประสงค์เพื่อเป็นเงื่อนไขในการเลือกไฟล์ ว่าเป็น database backup หรือไม่
 */
define('_MARK_ONE_','by_bps_script');

/**
 * ไฟล์ extension ของ backup file
 */
define('_BAK_EXT', 'bak');

/**
 * database configurartion
 */
define('_BACKUP_SERVER_','DESKTOP-C8P0LKD');
define('_BACKUP_USER_','backup');
define('_BACKUP_PASSWORD_','backup');
define('_BACKUP_SCRIPT_FILENAME_','backup.script.sql');

/**
 * FTP Config ของ ftp server ที่เราจะส่งไฟล์ไปเก็บเป็น secondary backup
 */
define('_FTP_SERVER_','119.59.122.138');
define('_FTP_USER_','mea2019');
define('_FTP_PASSWORD_','nadkk');