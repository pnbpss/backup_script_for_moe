
        /*
        Script written by Panu Boonpromsook. 20210203
        purpose of script is backing up databases to disk
        */
        use master;

        declare @script_name varchar(max), @datetimebak varchar(50),@fileNameAndPath varchar(max),@databaseName varchar(max), @backupName varchar(max), @filePath varchar(max);
        set @script_name = '.by_bps_script';
        set @filePath = 'D:\backup\dbs\dbfiles\'; 

        declare dbname_cursor cursor for SELECT [name]  FROM sys.databases where [name] in ('eec_jigsawoffice','tpqi_jigsawoffice','trd_jigsawoffice','trd_web','trd_mini','anamai_web','synerry_complaint','synerry_onesqa_old','synerry_onesqa_new','anamai.covid19','synerry_onesqa_converted','anamai_minisite','ieat_web','industry.law','migrate','industry_web','industry_minisite','industry_ops','distribution','TutorialDB');

        open dbname_cursor 
        fetch next from dbname_cursor into @databaseName 

        while @@FETCH_STATUS = 0
        begin
            set @datetimebak = convert(varchar(max),getdate(),112)+replace(convert(varchar(max),getdate(),108),':','');
            set @fileNameAndPath = @filePath+@datetimebak+'_'+@databaseName+ @script_name+'.bak';
            set @backupName = @databaseName+'-Full Database Backup (by_bps_script) '+@datetimebak;
            BACKUP DATABASE @databaseName TO  
            DISK = @fileNameAndPath
            WITH NOFORMAT,
            NOINIT,
            NAME = @backupName,
            SKIP, NOREWIND, NOUNLOAD,  STATS = 5
            fetch next from dbname_cursor into @databaseName 
        end
        GO
        CLOSE dbname_cursor;  
        DEALLOCATE dbname_cursor; 
        GO
        