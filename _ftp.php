<?php
Namespace App\Backupfile;
require_once('config.php');
class Ftp {
    public function __construct($server, $user, $pw){        
        $this->server = $server;
        $this->conn_id = ftp_connect($server,21,90);
        $this->login_result = ftp_login($this->conn_id, $user, $pw);
        if(!$this->login_result){
            echo PHP_EOL.'Unable to logged in '.$server;
        }else{
            echo PHP_EOL.'Logged in '.$server;
            #ftp_pasv ( $this->conn_id, true );
            echo ', "'.ftp_pwd($this->conn_id).'" is current DIR';
        }
    }

    public function send($file,$remoteFile, $mode = FTP_BINARY){        
        if(!$this->login_result){
            echo PHP_EOL.'ftp server not logged in';
        }
        $fp = fopen($file, 'r');
        echo PHP_EOL."Uloading.. $file";
        if (ftp_fput($this->conn_id, $remoteFile, $fp, $mode)) {
            echo PHP_EOL."Successfully uploaded $file";
        } else {
            echo PHP_EOL."There was a problem while uploading $file";
        }
        fclose($fp);
    }

    public function nb_send($file,$remoteFile, $mode = FTP_BINARY){        
        if(!$this->login_result){
            echo PHP_EOL.'ftp server not logged in';
        }
        $fp = fopen($file, 'r');
        echo PHP_EOL;
        $ret = ftp_nb_fput($this->conn_id, $remoteFile, $fp, $mode);
        $loop_count = 0; 
        $file_size = filesize($file);
        $chunk_size = 4096;
        $total_loops =  ($file_size / $chunk_size);
        echo " {$file} ".PHP_EOL;
        while($ret == FTP_MOREDATA){
            $loop_count++; 
            $percent_completed = floor(($loop_count* 100)/$total_loops);
            echo chr(13).""."Uloading.. ".$percent_completed.'%';
            $ret = ftp_nb_continue($this->conn_id);
        }
        if ($ret != FTP_FINISHED) {
            echo PHP_EOL."There was an error uploading the file...". $file;        
        }else{
            echo PHP_EOL."Successfully uploaded $file";
        }
            
        fclose($fp);
    }

    public function send_multifile($backed_up_files_ASC)
    {
        $backed_up_files = array_reverse($backed_up_files_ASC);
        $count_loop=1;
        foreach($backed_up_files as $backedup_file){
            $file_name_and_path = DB_FILES_PATH.$backedup_file['name'];
            if(\file_exists($file_name_and_path)){
                $this->nb_send($file_name_and_path,$backedup_file['dbname'].'.bak');
            }    
            $count_loop++;
            //ป้องกันไม่ให้ ftp ส่ง file ที่เก่ากว่า เพราะในบางครั้ง list ของ file ที่ส่งมาให้จะติดไฟล์เก่ามาด้วย
            if($count_loop > sizeof(_DB_NAMES_)){
                break;
            }
        }
    }

    public function chdir($dir)
    {
        if(ftp_chdir($this->conn_id, $dir)) {
            echo PHP_EOL."Current directory is now: " . ftp_pwd($this->conn_id) . "";
        } else { 
            echo PHP_EOL."Couldn't change directory to ".$dir;
        }
    }

    public function disconnect(){
        ftp_close($this->conn_id);        
        echo PHP_EOL.'Disconnected from '.$this->server;
    }
}